<?php
namespace App\Command;

use App\Model\Heading;
use App\Model\Plateau;
use App\Model\Position;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Model\Rover;

class RunCommand extends Command
{
    protected static $defaultName = 'app:run';

    protected function configure()
    {
        $this->addArgument('file', InputArgument::REQUIRED, 'scenario file name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('file');
        if (!file_exists($filename)) {
            throw new \ErrorException(sprintf('filename %s does not exists', $filename));
        }
        $ainstructions = explode("\n", file_get_contents($filename));
        $plateauString = array_shift($ainstructions);
        $plateauSize = explode(" ", $plateauString);

        $plateau = new Plateau($plateauSize[0], $plateauSize[1]);

        $aRovers = [];
        $isOdd = true;
        foreach ($ainstructions as $line) {
            if ($line) {
                if ($isOdd) {
                    $aRovers[count($aRovers)]['rover'] = $line;
                } else {
                    $aRovers[count($aRovers) - 1]['commands'] = $line;
                }
                $isOdd = !$isOdd;
            }
        }

        foreach ($aRovers as $aRover) {
            $roverPosition = explode(" ", $aRover['rover'], 3);
            $rover = new Rover($plateau, new Position($roverPosition[0], $roverPosition[1]), new Heading($roverPosition[2]));
            for ($i = 0; $i < strlen($aRover['commands']); $i++) {
                $rover->action(new \App\Model\Instruction($aRover['commands'][$i]));
            }
            $output->writeln(sprintf("%s %s %s", $rover->getPosition()->getX(),$rover->getPosition()->getY(),$rover->getHeading()->asString()));
        }
    }
}
