<?php

namespace App\Interfaces;


interface iInstruction
{
    const ACTION_MOVE = 'M';
    const ACTION_TURN_LEFT = 'L';
    const ACTION_TURN_RIGHT = 'R';

    public function isValid($command): bool;

    public function equal($string): bool;

    public function asString(): string;
}
