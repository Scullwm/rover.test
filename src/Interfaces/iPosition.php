<?php

namespace App\Interfaces;


interface iPosition
{
    function __construct($x, $y);

    function setPosition($x, $y);

    function getX();

    function getY();
}
